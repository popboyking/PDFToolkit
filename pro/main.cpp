﻿/**
 * @file main.cpp
 * Copyright (c) Gaaagaa. All rights reserved.
 * 
 * @author  : Gaaagaa
 * @date    : 2020-12-20
 * @version : 1.0.0.0
 * @brief   : 程序入口函数。
 */

#include "xtypes.h"
#include "XPDF_wrapper.h"
#include "MainWidget.h"

#include <QApplication>
#include <QMessageBox>

////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    x_int32_t xit_err = X_ERR_UNKNOW;

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication xApp(argc, argv);

    if (0 != XPDF_load("pdfium.dll", X_NULL))
    {
        QMessageBox::critical(
                    nullptr,
                    QString("软件错误"),
                    QString("程序加载 pdfium.dll 时失败，请确认其 是否存在 或 文件是否正确！"));
        return -1;
    }

    {
        MainWidget xWidget;
        xWidget.show();
        xit_err = xApp.exec();
    }

    XPDF_unload();

    return xit_err;
}

////////////////////////////////////////////////////////////////////////////////
