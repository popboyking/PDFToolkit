﻿/**
 * @file XWidgetJ2P.h
 * Copyright (c) Gaaagaa. All rights reserved.
 * 
 * @author  : Gaaagaa
 * @date    : 2020-12-20
 * @version : 1.0.0.0
 * @brief   : 实现 PDF转图片 面板部件。
 */

#ifndef XWIDGETJ2P_H
#define XWIDGETJ2P_H

#include <QWidget>

////////////////////////////////////////////////////////////////////////////////
// XWidgetJ2P

namespace Ui { class XWidgetJ2P; }

/**
 * @class XWidgetJ2P
 * @brief PDF转图片 面板部件。
 */
class XWidgetJ2P : public QWidget
{
    Q_OBJECT

    // constructor/destructor
public:
    explicit XWidgetJ2P(QWidget *parent = nullptr);
    ~XWidgetJ2P(void) override;

    // overrides
public:
    virtual void showEvent(QShowEvent *event) override;

    // public interfaces
public:

    // slots
private slots:
    void on_updateImageListCount(const QModelIndex &parent, int first, int last);
    void on_imageListCurrentRowChanged(const QModelIndex &current, const QModelIndex &previous);
    void on_btn_IListByFolder_clicked();
    void on_btn_IListByFile_clicked();
    void on_btn_IListRemove_clicked();
    void on_btn_IListCleanup_clicked();
    void on_comboBox_PageSize_currentIndexChanged(int index);
    void on_btn_PDFSave_clicked();

    // data members
private:
    Ui::XWidgetJ2P * ui;
};

////////////////////////////////////////////////////////////////////////////////

#endif // XWIDGETJ2P_H
