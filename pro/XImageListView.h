﻿/**
 * @file XImageListView.h
 * Copyright (c) Gaaagaa. All rights reserved.
 * 
 * @author  : Gaaagaa
 * @date    : 2020-12-20
 * @version : 1.0.0.0
 * @brief   : 图片列表显示的视图部件。
 */

#ifndef XIMAGELISTVIEW_H
#define XIMAGELISTVIEW_H

#include <QTreeView>
#include <QItemDelegate>
#include <QStandardItemModel>

#include "XJPEG_wrapper.h"

////////////////////////////////////////////////////////////////////////////////
// XImageListItemDelegate

/**
 * @class XImageItemDelegate
 * @brief XImageListView 对象的行绘制托管类。
 */
class XImageItemDelegate : public QItemDelegate
{
    Q_OBJECT

    // constructor/destructor
public:
    explicit XImageItemDelegate(QObject *parent = nullptr);
    virtual ~XImageItemDelegate(void) override;

    // overrides
public:
    void paint(QPainter *painter,
               const QStyleOptionViewItem &option,
               const QModelIndex &index) const override;

    QSize sizeHint(const QStyleOptionViewItem &option,
                   const QModelIndex &index) const override;
};

////////////////////////////////////////////////////////////////////////////////
// XImageListView

typedef std::pair< QString, QSize >  XImageInfo;
typedef std::vector< XImageInfo >    XImageInfoList;

/**
 * @class XImageListView
 * @brief 图片列表显示的视图部件。
 */
class XImageListView : public QTreeView
{
    Q_OBJECT

    // constructor/destructor
public:
    explicit XImageListView(QWidget *parent = nullptr);
    virtual ~XImageListView(void) override;

    // overrides
public:
    void dragEnterEvent(QDragEnterEvent *event) override;
    void dragMoveEvent(QDragMoveEvent *event) override;
    void dropEvent(QDropEvent *event) override;

    // public interfaces
public:
    /**********************************************************/
    /**
     * @brief 当前列表的行数。
     */
    int rowCount(void) const;

    /**********************************************************/
    /**
     * @brief 从指定文件夹导入各个 JPEG 图片文件路径信息，并显示到列表中。
     */
    int importFromDir(const QString & strDir);

    /**********************************************************/
    /**
     * @brief 导入多个 JPEG 图片文件路径信息，并显示到列表中。
     */
    int importImages(const QStringList & strImageFiles);

    /**********************************************************/
    /**
     * @brief 移除列表行。
     */
    bool remove(int row);

    /**********************************************************/
    /**
     * @brief 清空列表。
     */
    void cleanup(void);

    /**********************************************************/
    /**
     * @brief 获取指定行对应的图片文件路径信息。
     */
    QString getImagePath(int row) const;

    /**********************************************************/
    /**
     * @brief 读取整个列表的图片信息。
     */
    int queryImageList(XImageInfoList & xInfoList);

Q_SIGNALS:
    /**********************************************************/
    /**
     * @brief 信号：请求 加载指定行对应的图片尺寸信息，显示到列表中。
     */
    void requireSizeInfo(int row);

    // slots
public slots:
    /**********************************************************/
    /**
     * @brief 信号槽：加载指定行对应的图片尺寸信息，显示到列表中。
     */
    void on_loadSizeInfo(int row);

    // data members
protected:
    jdec_handle_t         m_jdecoder;   ///< JPEG 图片解码器（用于获取图片 宽高 信息）
    QStandardItemModel  * m_itemModel;  ///< 列表视图部件的 model 对象
};

////////////////////////////////////////////////////////////////////////////////

#endif // XIMAGELISTVIEW_H


