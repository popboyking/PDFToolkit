QT       += core gui
RC_ICONS += PDFToolkit.ico
VERSION   = 1.0.0.0

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

win32 {
    DEFINES += _CRT_SECURE_NO_WARNINGS
}

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

QMAKE_CXXFLAGS += -execution-charset:utf-8

win32-msvc* {
    QMAKE_CXXFLAGS += -source-charset:utf-8
}

INCLUDEPATH += $$PWD/../libjpeg/inc $$PWD/../src

SOURCES += \
    ../libjpeg/src/jaricom.c \
    ../libjpeg/src/jcapimin.c \
    ../libjpeg/src/jcapistd.c \
    ../libjpeg/src/jcarith.c \
    ../libjpeg/src/jccoefct.c \
    ../libjpeg/src/jccolor.c \
    ../libjpeg/src/jcdctmgr.c \
    ../libjpeg/src/jchuff.c \
    ../libjpeg/src/jcinit.c \
    ../libjpeg/src/jcmainct.c \
    ../libjpeg/src/jcmarker.c \
    ../libjpeg/src/jcmaster.c \
    ../libjpeg/src/jcomapi.c \
    ../libjpeg/src/jcparam.c \
    ../libjpeg/src/jcprepct.c \
    ../libjpeg/src/jcsample.c \
    ../libjpeg/src/jctrans.c \
    ../libjpeg/src/jdapimin.c \
    ../libjpeg/src/jdapistd.c \
    ../libjpeg/src/jdarith.c \
    ../libjpeg/src/jdatadst.c \
    ../libjpeg/src/jdatasrc.c \
    ../libjpeg/src/jdcoefct.c \
    ../libjpeg/src/jdcolor.c \
    ../libjpeg/src/jddctmgr.c \
    ../libjpeg/src/jdhuff.c \
    ../libjpeg/src/jdinput.c \
    ../libjpeg/src/jdmainct.c \
    ../libjpeg/src/jdmarker.c \
    ../libjpeg/src/jdmaster.c \
    ../libjpeg/src/jdmerge.c \
    ../libjpeg/src/jdpostct.c \
    ../libjpeg/src/jdsample.c \
    ../libjpeg/src/jdtrans.c \
    ../libjpeg/src/jerror.c \
    ../libjpeg/src/jfdctflt.c \
    ../libjpeg/src/jfdctfst.c \
    ../libjpeg/src/jfdctint.c \
    ../libjpeg/src/jidctflt.c \
    ../libjpeg/src/jidctfst.c \
    ../libjpeg/src/jidctint.c \
    ../libjpeg/src/jmemmgr.c \
    ../libjpeg/src/jmemnobs.c \
    ../libjpeg/src/jquant1.c \
    ../libjpeg/src/jquant2.c \
    ../libjpeg/src/jutils.c \
    ../src/XJPEG_wrapper.c \
    ../src/XPDF_wrapper.cpp \
    XImageListView.cpp \
    XImageView.cpp \
    XWidgetJ2P.cpp \
    XWidgetP2J.cpp \
    main.cpp \
    MainWidget.cpp

HEADERS += \
    ../libjpeg/inc/jconfig.h \
    ../libjpeg/inc/jdct.h \
    ../libjpeg/inc/jerror.h \
    ../libjpeg/inc/jinclude.h \
    ../libjpeg/inc/jmemsys.h \
    ../libjpeg/inc/jmorecfg.h \
    ../libjpeg/inc/jpegint.h \
    ../libjpeg/inc/jpeglib.h \
    ../libjpeg/inc/jversion.h \
    ../src/XJPEG_wrapper.h \
    ../src/XPDF_wrapper.h \
    ../src/xtypes.h \
    MainWidget.h \
    XImageListView.h \
    XImageView.h \
    XWidgetJ2P.h \
    XWidgetP2J.h

FORMS += \
    MainWidget.ui \
    XWidgetJ2P.ui \
    XWidgetP2J.ui

RESOURCES += \
    PDFToolkit.qrc

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

