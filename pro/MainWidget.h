﻿/**
 * @file MainWidget.h
 * Copyright (c) Gaaagaa. All rights reserved.
 * 
 * @author  : Gaaagaa
 * @date    : 2020-12-20
 * @version : 1.0.0.0
 * @brief   : 程序主窗口部件。
 */

#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>

////////////////////////////////////////////////////////////////////////////////
// MainWidget

QT_BEGIN_NAMESPACE
namespace Ui { class MainWidget; }
QT_END_NAMESPACE

/**
 * @class MainWidget
 * @brief 程序主窗口部件类。
 */
class MainWidget : public QWidget
{
    Q_OBJECT

    // constructor/destructor
public:
    MainWidget(QWidget *parent = nullptr);
    ~MainWidget(void);

    // overrides : events
protected:
    virtual void closeEvent(QCloseEvent *event);

    // data members
private:
    Ui::MainWidget * ui;
};

////////////////////////////////////////////////////////////////////////////////

#endif // MAINWIDGET_H
