﻿/**
 * @file MainWidget.cpp
 * Copyright (c) Gaaagaa. All rights reserved.
 * 
 * @author  : Gaaagaa
 * @date    : 2020-12-20
 * @version : 1.0.0.0
 * @brief   : 程序主窗口部件。
 */

#include "MainWidget.h"
#include "ui_MainWidget.h"

#include <QCloseEvent>
#include <QMessageBox>

////////////////////////////////////////////////////////////////////////////////
// MainWidget

//====================================================================

// 
// MainWidget : constructor/destructor
// 

MainWidget::MainWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::MainWidget)
{
    ui->setupUi(this);

    setWindowIcon(QIcon(tr(":/PDFToolkit.png")));
}

MainWidget::~MainWidget(void)
{
    delete ui;
}

//====================================================================

//
// MainWidget : override events
//

void MainWidget::closeEvent(QCloseEvent *event)
{
    //======================================

    if (ui->tab_PDF2Jpeg->is_working())
    {
        ui->tabWidget->setCurrentIndex(0);
        if (QMessageBox::No ==
                QMessageBox::question(
                    this,
                    tr("提示"),
                    tr("当前还有任务在执行中，是否要强制退出程序？")))
        {
            event->ignore();
            return;
        }
    }

    //======================================

    event->accept();

    //======================================
}


