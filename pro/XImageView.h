﻿/**
 * @file XImageView.h
 * Copyright (c) Gaaagaa. All rights reserved.
 *
 * @author  : Gaaagaa
 * @date    : 2020-12-20
 * @version : 1.0.0.0
 * @brief   : 图片预览的视图部件。
 */

#ifndef XIMAGEVIEW_H
#define XIMAGEVIEW_H

#include <QWidget>
#include <QImage>

////////////////////////////////////////////////////////////////////////////////
// XImageView

/**
 * @class XImageView
 * @brief 图片预览的视图部件。
 */
class XImageView : public QWidget
{
    Q_OBJECT

    // constructor/destructor
public:
    explicit XImageView(QWidget *parent = nullptr);
    ~XImageView(void);

    // overrides
protected:
    virtual void paintEvent(QPaintEvent *event) override;
    virtual void mouseDoubleClickEvent(QMouseEvent *event) override;

    // public interfaces
public:
    /**********************************************************/
    /**
     * @brief 当前图片文件路径。
     */
    inline const QString & filePath(void) const
    {
        return m_strPath;
    }

    /**********************************************************/
    /**
     * @brief 当前已经加载显示的图片对象。
     */
    inline const QImage & image(void) const
    {
        return m_imgShow;
    }

    /**********************************************************/
    /**
     * @brief 设置当前显示的图片文件。
     */
    bool setCurrent(const QString & strFilePath);

    // data members
protected:
    QString m_strPath;  ///< 图片文件路径
    QImage  m_imgShow;  ///< 显示的图片对象

};

////////////////////////////////////////////////////////////////////////////////

#endif // XIMAGEVIEW_H
