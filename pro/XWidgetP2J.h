﻿/**
 * @file XWidgetP2J.h
 * Copyright (c) Gaaagaa. All rights reserved.
 * 
 * @author  : Gaaagaa
 * @date    : 2020-12-20
 * @version : 1.0.0.0
 * @brief   : 实现 PDF转图片 面板部件。
 */

#ifndef XWIDGETP2J_H
#define XWIDGETP2J_H

#include "xtypes.h"
#include "XPDF_wrapper.h"
#include "XJPEG_wrapper.h"
#include <QWidget>

#include <thread>

////////////////////////////////////////////////////////////////////////////////
// XWidgetP2J

namespace Ui { class XWidgetP2J; }

/**
 * @class XWidgetP2J
 * @brief PDF转图片 面板部件。
 */
class XWidgetP2J : public QWidget
{
    Q_OBJECT

    // common data types
protected:
    using xvec_no_t = std::vector< x_int32_t >;

    // constructor/destructor
public:
    explicit XWidgetP2J(QWidget* parent = nullptr);

    ~XWidgetP2J(void) override;

public:
    inline bool is_working() const { return (X_TRUE == m_xbt_contine); }

    // inner invoking
protected:
    void enableCtrl(bool bEnable);
    bool queryPageNoQueue(xvec_no_t & xvec_no);
    bool queryOutDir(std::string & xstr_dir);

    void thread_work(
            const xvec_no_t   & xvec_no,
            x_lfloat_t          xlft_zoom,
            x_uint32_t          xut_mcx,
            x_uint32_t          xut_mcy,
            const std::string & xstr_dir);

    // signals
Q_SIGNALS:
    void reportInfo(int nCount, int nIndex, bool isError, const QString & strInfo);
    void workFinished();

    // slots
private slots:
    void on_reportInfo(int nCount, int nIndex, bool isError, const QString & strInfo);
    void on_workFinished();
    void on_btn_PDFFile_clicked();
    void on_btn_PDFFileReset_clicked();
    void on_comboBox_PageRange_currentIndexChanged(int index);
    void on_checkBox_JpegMaxSize_toggled(bool checked);
    void on_btn_OutDir_clicked();
    void on_btn_OutStart_clicked();
    void on_btn_OutCancel_clicked();

    // data members
private:
    Ui::XWidgetP2J * ui;

    XPDF_reader_t      m_xpdf_reader;   ///< PDF 文件读操作的工作对象
    jenc_handle_t      m_jenc_handle;   ///< JPEG 编码器

    std::thread        m_xtrd_worker;   ///< 工作线程对象
    volatile x_bool_t  m_xbt_contine;   ///< 标识工作线程是否可继续执行
};

////////////////////////////////////////////////////////////////////////////////

#endif // XWIDGETP2J_H
