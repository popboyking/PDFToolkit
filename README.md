## 开源一个 PDF 小工具集软件【使用 PDFium 库实现】

## 1. 为什么要写这软件

&emsp;&emsp;前些天，在网上下到了一本 PDF 电子书，是扫描版的，页面有些模糊。想着，要是能把所有页面都导出成一堆图片，再写个批处理命令，用 ffmpeg 逐张图片进行 **锐化** 操作，这可以使得图片显示起来清晰一些，最后就是把所有图片再合并成 PDF 文件。这样一波操作下来，这本 PDF 电子书，看着就舒服很多了。

&emsp;&emsp;**PDF转图片** 和 **图片合并成PDF** 这样的小工具软件有很多，但可惜的是，要么要注册码，要么就是充会员付费，使用免费功能部分，就会有 ***加水印***、***低分辨率*** 这类恶心人的操作。

&emsp;&emsp;好在我以前写的程序中，有用 PDFium 操作 PDF 文件的代码，花了点业余时间，用 QT 扒拉出 UI 界面来，这个 PDF 小工具集软件就弄成了。本着开源精神，也为他人提供方便，就把这软件贡献出来了！！！

## 2. 这软件长啥样

### 2.1 PDF 导出到 图片 的功能

![](./bin/P2JE.png)

&emsp;&emsp;温馨提示：图中所示，设置 **输出图片的缩放倍率** 为 3 倍时，和 **WPS** 的会员功能 **高清品质（300%）** 一个意思了！！！

### 2.2 图片合并到 PDF 的功能

![](./bin/J2PE.png)

## 3. 到这里下载

&emsp;&emsp;转到我 gitee 中的代码仓库位置：[https://gitee.com/Gaaagaa/PDFToolkit](https://gitee.com/Gaaagaa/PDFToolkit) ，在 **bin** 目录下，有 64位 和 32位 两个版本。整个压缩包文件有点大，20M 左右，这是 QT 的锅！！！

&emsp;&emsp;另外，需要说明的三点：
1. 我使用的 PDFium 库，是从 [https://github.com/bblanchon/pdfium-binaries](https://github.com/bblanchon/pdfium-binaries) 拿到已编译好的 dll 文件。自己编译，会有很多坑等着你去踩的！！！
2. 软件的代码中，使用 PDFium 库，是通过动态加载 pdfium.dll 后，获取相关操作的函数指针来实现后续操作。这一行为，有可能被杀软视为危险操作，这可以不在意它。
3. 下载该软件时，请直接在我上面给出的链接下载。其他地方下载到的，有可能拿到挂马的程序。

## 4. 后记

&emsp;&emsp;当下，这个小工具只有 **PDF转图片** 和 **图片合并成PDF** 这两种功能，以后有时间，我会考虑增加 **编辑 PDF 导航标签/目录** 的功能的。我编辑 PDF 目录时，用的是 **PdgCntEditor** 这个工具（骨灰级的东西了）。

&emsp;&emsp;另外，若是喜欢命令行工具的，可以编译我写的两个测试程序的代码，即 **test** 目录下的 **extract.cpp** 和 **combine.cpp** ，分别实现了 **PDF转图片** 和 **图片合并成PDF** 这两功能。

&emsp;&emsp;最后，附赠 ffmpeg 锐化图片的批处理脚本，如下：
> ```
> @echo off
> setlocal enabledelayedexpansion
> mkdir sharpen_out
> set n=1
> for /f %%i in ('dir /b *.jpg') do (
>     ffmpeg -i "%%i" -vf unsharp=13:13:5:13:13:5 sharpen_out/"%%i"
>     set /a n+=1
> )
> echo: 
> echo Sharpen finished.
> echo: 
> @echo on
> ```

